const express = require("express");

// Create an application using express
const app = express();

// App server listening port
const port = 3000;

// Setup for allowing the server to handle data from requests
// Allows the app to read json data
app.use(express.json());

// Allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

// GET Route
app.get("/", (req, res) =>{
	res.send("Hello World");
});

app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!");
});

// POST Route
app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// Mock Database
let users = [];

// Will create a signup POST
app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfuly registered`);
	}

	else{
		res.send("Please input BOTH username and password");
	}
});

// PUT request for changing password
app.put("/change-password", (req, res) => {
	// Creates a variable to store the message to be sent back to the client(Postman)
	let message;

	// Creates a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++){
		if(req.body.username === users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`;
			break;
		}

		else{
			message = "User does not exist."
		}
	}

	res.send(message);
});

app.get("/home", (req, res) => {
	res.send("Welcome to the home page.");
});

app.get("/users", (req, res) => {
	let json = users.json();
	res.send(`${json}`);
});

app.delete("/delete-user", (req, res) => {
	let message;

	for(let i = 0; i < users.length; i++){
		if(req.body.username === users[i].username){
			req.body.username.splice();
			message = `User ${req.body.username} has been deleted`;
			break;
		}

		else{
			message = "User does not exist."
		}
	}

	res.send(message);
});



// Tells the server to listen to the port
app.listen(port, () => console.log(`Server is running at port ${port}`));